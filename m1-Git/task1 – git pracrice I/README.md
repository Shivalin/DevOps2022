# GIT Basics homework
## Part 0

### Pass  https://learngitbranching.js.org/

<img src="/m1-Git/images/git-0-02.png" width="600"><br>
<img src="/m1-Git/images/git-0-01.png" width="600"><br>

### Pass Git course	https://learn.epam.com/detailsPage?id=16d7f2e7-cc80-4870-928e-400723f732bb

<img src="/m1-Git/images/git-0-03.png" width="600"><br>
## Part 1

### 1. Install GIT on your workstation
>done

### 2. Setup git: change your global configs (add name and email, setup core text editor).
>done

<img src="/m1-Git/images/git-1-01.png" width="600"><br>

### 3. Create account on GitLab
>done

### 4. Generate ssh-key and integrate it with GitLab.
>I have generated ed25519 key
>I have added public part of the key to my github account with expiration date 30.09.2022

```
ssh-keygen -t ed25519 -C "shadrin.slava@gmail.com"
```

### 5. Create new project on GitLab.
>done

### 6. Clone project to your workstation.

```
cd d:/DevOps/
mkdir gitlab
cd d:/DevOps/gitlab/
git clone https://gitlab.com/Shivalin/DevOps2022.git
git remote add origin https://gitlab.com/Shivalin/DevOps2022.git

```

### 7. Create .gitignore file. Include .gitignore in its own list. Try to use different patterns in
.gitignore file (see in lecture) to make invisible for git some local files that should not be
commited. Check that files mentioned in .gitignore file shouldn’t be visible via executing git
status command.

```
*.log
*.db
m1-Git/task2\ –\ git pracrice II/
```





