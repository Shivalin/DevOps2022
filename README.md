# EPAM DevOps Spring 2022
Author: Viacheslav Shadrin

## m1-Git

- [ ] [Task0 and Task1](./m1-Git/task1\ –\ git pracrice I/README.md)
- [ ] [Task2](./m1-Git-Task2/README.md)\

## m2-Python

- [ ] [Task1](./m2-Python-Task-01/README.md)
- [ ] [Task2](./m2-Python-Task-02/README.md)
- [ ] [Task3](./m2-Python-Task-03/README.md)
- [ ] [Task4](./m2-Python-Task-04/README.md)
- [ ] [Task5](./m2-Python-Task-05/README.md)

## m3-Linux

- [ ] [Task1](./m3-Linux-Task-01/README.md)
- [ ] [Task1](./m3-Linux-Task-02/README.md)
- [ ] [Task1](./m3-Linux-Task-03/README.md)

